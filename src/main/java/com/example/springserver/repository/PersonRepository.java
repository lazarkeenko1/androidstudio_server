package com.example.springserver.repository;

import com.example.springserver.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PersonRepository extends JpaRepository<Person, Long> {
    @Query("select p from Person p where p.id = ?1")
    Person findPersonById(Long id);

    @Query("select p from Person p")
    List<Person> getAllPersons();
}
