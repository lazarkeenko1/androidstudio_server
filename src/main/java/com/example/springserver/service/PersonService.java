package com.example.springserver.service;

import com.example.springserver.controller.PersonController;
import com.example.springserver.entity.Person;
import com.example.springserver.repository.PersonRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person getPersonById(Long id) {
        return personRepository.findPersonById(id);
    }
    public List<Person> getPersons() {
        return personRepository.getAllPersons();
    }
    public Person savePerson(Person person){
        personRepository.save(person);
        return person;
    }
}
