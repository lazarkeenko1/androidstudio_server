package com.example.springserver.controller;

import com.example.springserver.entity.Person;
import com.example.springserver.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(path = { "/person"})
public class PersonController {
    PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/byId/{id}")
    public ResponseEntity<Person> getPersonById(@PathVariable("id") Long id){
        Person person = personService.getPersonById(id);
        return new ResponseEntity<>(person, OK);
    }
    @GetMapping("/all")
    public ResponseEntity<List<Person>> getPersons(){
        List<Person> persons = personService.getPersons();
        return new ResponseEntity<>(persons, OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Person> savePerson(@RequestBody Person person){
        Person personResponse = personService.savePerson(person);
        return new ResponseEntity<>(personResponse, OK);
    }
}
